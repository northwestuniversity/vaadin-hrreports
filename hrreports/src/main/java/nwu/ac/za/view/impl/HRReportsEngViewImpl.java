package nwu.ac.za.view.impl;

import com.vaadin.ui.*;

import nwu.ac.za.designer.generated.DIYReporting;
import nwu.ac.za.view.infc.HRReportsInfc;

public class HRReportsEngViewImpl extends DIYReporting implements HRReportsInfc {

	@Override
	public VerticalLayout getVerticalLayout() {
		return verticalLayout;
	}

	@Override
	public void setVerticalLayout(VerticalLayout verticalLayout) {

	}

	@Override
	public Label getUnivNumber() {
		return univNumber;
	}

	@Override
	public void setUnivNumber(Label univNumber) {

	}

	@Override
	public TextField getOutputUniversityNumber() {
		return outputUniversityNumber;
	}

	@Override
	public void setOutputUniversityNumber(TextField outputUniversityNumber) {

	}

	@Override
	public Label getOutputUniversityNumberLabel() {
		return outputUniversityNumberLabel;
	}

	@Override
	public void setOutputUniversityNumberLabel(Label outputUniversityNumberLabel) {

	}

	@Override
	public Label getOutputTitleInitialsSurname() {
		return outputTitleInitialsSurname;
	}

	@Override
	public void setOutputTitleInitialsSurname(Label outputTitleInitialsSurname) {

	}

	@Override
	public Label getReportParametersLabel() {
		return reportParametersLabel;
	}

	@Override
	public void setReportParametersLabel(Label reportParametersLabel) {

	}

	@Override
	public HorizontalLayout getOeHorizontalLayout() {
		return oeHorizontalLayout;
	}

	@Override
	public void setOeHorizontalLayout(HorizontalLayout oeHorizontalLayout) {

	}

	@Override
	public ComboBox getoEDropdown() {
		return oEDropdown;
	}

	@Override
	public void setoEDropdown(ComboBox oEDropdown) {

	}

	@Override
	public HorizontalLayout getTableHorizontalLayout() {
		return tableHorizontalLayout;
	}

	@Override
	public void setTableHorizontalLayout(HorizontalLayout tableHorizontalLayout) {

	}

	@Override
	public Table getReportSelectionTable() {
		return reportSelectionTable;
	}

	@Override
	public void setReportSelectionTable(Table reportSelectionTable) {

	}
}
