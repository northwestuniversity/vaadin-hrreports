package nwu.ac.za.view.infc;

import com.vaadin.ui.*;

public interface HRReportsInfc {

	VerticalLayout getVerticalLayout();
	void setVerticalLayout(VerticalLayout verticalLayout);
	Label getUnivNumber();
	void setUnivNumber(Label univNumber);
	TextField getOutputUniversityNumber();
	void setOutputUniversityNumber(TextField outputUniversityNumber);
	Label getOutputUniversityNumberLabel();
	void setOutputUniversityNumberLabel(Label outputUniversityNumberLabel);
	Label getOutputTitleInitialsSurname();
	void setOutputTitleInitialsSurname(Label outputTitleInitialsSurname);
	Label getReportParametersLabel();
	void setReportParametersLabel(Label reportParametersLabel);
	HorizontalLayout getOeHorizontalLayout();
	void setOeHorizontalLayout(HorizontalLayout oeHorizontalLayout);
	ComboBox getoEDropdown();
	void setoEDropdown(ComboBox oEDropdown);
	public HorizontalLayout getTableHorizontalLayout();
	void setTableHorizontalLayout(HorizontalLayout tableHorizontalLayout);
	Table getReportSelectionTable();
	void setReportSelectionTable(Table reportSelectionTable);
}
