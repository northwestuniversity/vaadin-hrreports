package nwu.ac.za;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.servlet.annotation.WebServlet;

import client.supervisorhierarchyinformation.supervisorhierarchyinformationprocess.*;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.data.Property;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.Field;
import com.vaadin.ui.Label;
import com.vaadin.ui.Table;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

import ac.za.nwu.core.person.dto.PersonBiographicInfo;
import ac.za.nwu.core.person.service.PersonService;
import ac.za.nwu.core.person.service.factory.PersonServiceClientFactory;
import ac.za.nwu.utility.EncryptorUtility;
import assemble.edu.common.dto.ContextInfo;
import assemble.edu.exceptions.DoesNotExistException;
import assemble.edu.exceptions.InvalidParameterException;
import assemble.edu.exceptions.MissingParameterException;
import assemble.edu.exceptions.OperationFailedException;
import assemble.edu.exceptions.PermissionDeniedException;
import client.employeeinformation.EmployeeAssignmentDetails;
import client.employeeinformation.EmployeeDetailRequest;
import client.employeeinformation.EmployeeInformation;
import client.employeeinformation.HRCOILookupInformation;
import client.employeeinformation.HRLookupInformationRequest;
import client.reportinginformation.DIYReportingProcess;
import client.reportinginformation.ReportInput;
import client.reportinginformation.ReportRequestResponse;
import nwu.ac.za.framework.ServiceRegistryLookupUtility;
import nwu.ac.za.framework.SinglePageCRUDUI;
import nwu.ac.za.ui.utils.exceptions.VaadinUIException;
import nwu.ac.za.ui.utils.exceptions.VaadinUIException.SeverityType;
import nwu.ac.za.ui.utils.exceptions.VaadinUIException.Type;
import nwu.ac.za.util.HRReportsConstants;
import nwu.ac.za.util.HRReportsMessages;
import nwu.ac.za.util.PropertyValueRetriever;
import nwu.ac.za.view.impl.HRReportsAfrViewImpl;
import nwu.ac.za.view.impl.HRReportsEngViewImpl;
import nwu.ac.za.view.infc.HRReportsInfc;
import org.apache.commons.lang.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This UI is the application entry point. A UI may either represent a browser window
 * (or tab) or some part of a html page where a Vaadin application is embedded.
 * <p>
 * The UI is initialized using {@link #init(VaadinRequest)}. This method is intended to be
 * overridden to add component to the user interface and initialize non-component functiona lity.
 */
@SuppressWarnings("serial")
@Theme("tests-valo-facebook")
public class HRReportsSPCrudUI extends SinglePageCRUDUI {

    private final Logger log = LoggerFactory.getLogger(HRReportsSPCrudUI.class.getName());

    private static final String HR_REPORTS_SUBSCRIBER = "HRREPORTS";
    private HRReportsInfc contentViewDetail;
    private PersonService personService;
    private EmployeeInformation hrService;
    private EmployeeAssignmentDetails.EmployeeRecord employeeRecord;
    private PersonBiographicInfo personBiographicInfo;

    private PropertyValueRetriever propertyValueRetriever = new PropertyValueRetriever();
    private SupervisorHierarchyInformationProcess supervisorHierarchyInformationProcess;

    private DIYReportingProcess reportingService;
    private List<ReportInput.ReportRecord> reports;
    private ReportInput reportInput;
    private ReportRequestResponse reportRequestResponse;

    private String serviceLang;
    private String appRuntimeEnvironment;
    private static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    private String wsIAPIReadUser = null;
    private String wsIAPIReadUserPassword = null;

    private ContextInfo hrReportContextInfo;
    private String hrPassword;
    // define security keys and initVector
    private static final String SECKEY = "Bar12345Bar12345";
    private static final String INITVECTOR = "RandomInitVector";
    private List<String> listOeDetail = new ArrayList<String>();

    private String pOSApprover;

    @Override
    public void afterUIInitialization() {
        this.contentViewDetail.getoEDropdown().setNewItemsAllowed(false);
        this.contentViewDetail.getoEDropdown().setTextInputAllowed(true);
        this.contentViewDetail.getoEDropdown().setNullSelectionAllowed(false);

        createTable();
        showHideReportTable(false);
        this.hrReportContextInfo = new ContextInfo(HR_REPORTS_SUBSCRIBER);
        loadInitialData();
    }

    @Override
    public String getBrowserTabTitle() {
        return (HRReportsConstants.APP_NAME);
    }

    @Override
    protected void setDetailView() {
        String lang = UI.getCurrent().getLocale().getLanguage();
        VerticalLayout view;

        if (lang.equals(HRReportsConstants.APP_LANGUAGE_AF)) {
            view = new HRReportsAfrViewImpl();
            serviceLang = HRReportsConstants.MESSAGE_CATALOGUE_AF_NAME;
        } else {
            view = new HRReportsEngViewImpl();
            serviceLang = HRReportsConstants.MESSAGE_CATALOGUE_ENG_NAME;
        }
        this.detailContent = view;
        contentViewDetail = (HRReportsInfc) this.detailContent;

        pOSApprover = getPOSApprover();
    }

    @Override
    protected void registerEvents() {

    }

    @Override
    protected void serviceInitialization() {
        appRuntimeEnvironment = setupAppRuntimeEnvironment();
        try {
            String personServiceLookupKey = ServiceRegistryLookupUtility.getServiceRegistryLookupKey(PersonServiceClientFactory.PERSONSERVICE, HRReportsConstants.PROPERTY_NAME_IDENTITY_API_VERSION, null, HRReportsConstants.PROPERTY_FILE_NAME, appRuntimeEnvironment);

            wsIAPIReadUser = propertyValueRetriever.getPropertyValues
                    (HRReportsConstants.WS_IAPI_READ_USER_NAME, HRReportsConstants.PROPERTY_FILE_NAME);
            String encyptedPassword = propertyValueRetriever.getPropertyValues(HRReportsConstants.WS_IAPI_READ_USER_NAME_PASSWORD,
                    HRReportsConstants.PROPERTY_FILE_NAME);
            wsIAPIReadUserPassword = EncryptorUtility.decrypt(SECKEY, INITVECTOR, encyptedPassword);

            personService = PersonServiceClientFactory.getPersonService(personServiceLookupKey, wsIAPIReadUser, wsIAPIReadUserPassword);
        } catch (Exception e) {
            throw new VaadinUIException(e, SeverityType.ERROR, null, VaadinUIException.Type.SERVERSTARTUPFAILURE, false);
        }

        //HR Employee Information Service Initialization
        String hrEncryptedPassword;
        try {
            hrEncryptedPassword = propertyValueRetriever.getPropertyValues(HRReportsConstants.PASSWORD, HRReportsConstants.PROPERTY_FILE_NAME);
            hrPassword = EncryptorUtility.decrypt(SECKEY, INITVECTOR, hrEncryptedPassword);
        } catch (IOException e1) {
            throw new VaadinUIException(e1, SeverityType.ERROR, null, VaadinUIException.Type.SERVERSTARTUPFAILURE, false);
        }

        try {

            String majorVersion = getMajorVersion(HRReportsConstants.getHR_EMPLOYEE_INFO_SERVICE_VERSION());
//            String hremployeelookupkey = getHRServiceRegistryEnvironmentTypeKey(HREmployeeInformationServiceClientFactory.EMPLOYEEINFORMATION_SERVICE);

            String hremployeelookupkey = "/"
                    .concat(appRuntimeEnvironment.toUpperCase()).concat("/")
                    .concat(HREmployeeInformationServiceClientFactory.EMPLOYEEINFORMATION_SERVICE).concat("/")
                    .concat(majorVersion);
            
            
            hrService = HREmployeeInformationServiceClientFactory.getEmployeeInformationService(hremployeelookupkey, propertyValueRetriever.getPropertyValues
                    (HRReportsConstants.USER_NAME, HRReportsConstants.PROPERTY_FILE_NAME), hrPassword);
        } catch (IOException | DoesNotExistException | MissingParameterException | OperationFailedException | PermissionDeniedException e) {
            throw new VaadinUIException(e, SeverityType.ERROR, null, VaadinUIException.Type.SERVERSTARTUPFAILURE, false);
        }

        //HR Supervisor Hierarchy Service Initialization
        try {
            String supervisorLookupKey = getHRServiceRegistryEnvironmentTypeKey(HRSupervisorHierarchyInformationServiceClientFactory.SUPERVISORHIERARCHYINFORMATION_SERVICE);
            supervisorHierarchyInformationProcess = HRSupervisorHierarchyInformationServiceClientFactory.getSupervisorHierarchyInformationService(supervisorLookupKey, propertyValueRetriever.getPropertyValues(HRReportsConstants.USER_NAME,
                    HRReportsConstants.PROPERTY_FILE_NAME), hrPassword);
        } catch (IOException | DoesNotExistException | MissingParameterException | OperationFailedException e) {
            throw new VaadinUIException(e, SeverityType.ERROR, null, VaadinUIException.Type.SERVERSTARTUPFAILURE, false);

        }

        //HR Reporting Information Service Initialization
        try {
            String diyreportingLookupKey = getHRServiceRegistryEnvironmentTypeKey(HRReportingInformationServiceClientFactory.DIYREPORTINGSERVICE_SERVICE);
            reportingService = HRReportingInformationServiceClientFactory.getReportingInformationService
                    (diyreportingLookupKey, propertyValueRetriever.getPropertyValues(HRReportsConstants.USER_NAME,
                            HRReportsConstants.PROPERTY_FILE_NAME), hrPassword);
        } catch (IOException | DoesNotExistException | MissingParameterException | OperationFailedException e) {
            throw new VaadinUIException(e, SeverityType.ERROR, null, VaadinUIException.Type.SERVERSTARTUPFAILURE, false);
        }
    }


    private String setupAppRuntimeEnvironment() {
        String env = "test";
        try {
            env = propertyValueRetriever.getPropertyValues(HRReportsConstants.RUN_ENVIRONMENT, HRReportsConstants.PROPERTY_FILE_NAME);
        } catch (IOException e) {
            throw new VaadinUIException(e, SeverityType.ERROR, null, VaadinUIException.Type.SERVERSTARTUPFAILURE, false);
        }
        return env;
    }

    @Override
    public void registerSinglePageEvents() {
        this.contentViewDetail.getOutputUniversityNumber().addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            public void valueChange(Property.ValueChangeEvent event) {
                Property univNumberTextField = event.getProperty();
                String value = (String) univNumberTextField.getValue();
                if (value != null) {
                    resetUI();
                    lookupUser = value;
                    loadInitialData();
                }
            }
        });

        this.contentViewDetail.getoEDropdown().addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            public void valueChange(Property.ValueChangeEvent event) {
                String selectedValue = (String) event.getProperty().getValue();
                clearTable();
                if (selectedValue != null) {
                    showHideReportTable(true);
                    populateTable();
                } else {
                    showHideReportTable(false);
                }
            }
        });
    }

    @Override
    public void beforeSaveValidations() {

    }

    @Override
    public void afterSave() {
        if (!reportRequestResponse.isErrorflag()) {
            throw new VaadinUIException(HRReportsMessages.getString(HRReportsConstants.MESSAGE_CATALOGUE_REPORTSUCCESS, serviceLang), SeverityType.SUCCESS, Type.UNKNOWN);
        } else {
            if (reportRequestResponse.getErrorMessage().equals(HRReportsMessages.getString(HRReportsConstants.MESSAGE_CATALOGUE_NOREPORTS, serviceLang))) {
                throw new VaadinUIException(HRReportsMessages.getString(HRReportsConstants.MESSAGE_CATALOGUE_REPORTERROR, serviceLang), SeverityType.ERROR, Type.UNKNOWN);
            } else {
                throw new VaadinUIException(reportRequestResponse.getErrorMessage(), SeverityType.WARNING, Type.UNKNOWN);
            }
        }
    }

    @Override
    public void beforeSave() {
        reportInput = new ReportInput();
        reports = reportInput.getReportRecord();
        for (Object id : contentViewDetail.getReportSelectionTable().getItemIds()) {
            if (getSelectCheckBoxValue((int) id, HRReportsMessages.getString(HRReportsConstants.MessageCatalogue.SELECT_COLUMN_NAME, serviceLang))) {
                ReportInput.ReportRecord reportRecord = new ReportInput.ReportRecord();
                reportRecord.setReportName(getLabelValue((int) id, HRReportsMessages.getString(HRReportsConstants.MessageCatalogue.REPORT_COLUMN_NAME, serviceLang)));
                reportRecord.setEmployeeNumber(lookupUser);

                if(contentViewDetail.getoEDropdown().getValue().toString().contains("-")){
                    String[] oeCode = contentViewDetail.getoEDropdown().getValue().toString().split(" - ");
                    reportRecord.setArgument1(oeCode[0]);
                }else{
                    reportRecord.setArgument1(contentViewDetail.getoEDropdown().getValue().toString());
                }

                reports.add(reportRecord);
            }
        }
    }

    @Override
    public void save() {
        reportRequestResponse = reportingService.process(reportInput);
    }

    @Override
    public void loadInitialData() {
        try {
            personBiographicInfo = personService.getPersonBiographic(lookupUser, this.hrReportContextInfo);
        } catch (DoesNotExistException | InvalidParameterException | OperationFailedException |
                MissingParameterException | PermissionDeniedException e) {
            throw new VaadinUIException(e, SeverityType.ERROR, null, VaadinUIException.Type.SERVERSIDE, false);
        }

        mapDTOtoUIComponents();
    }

    public void setReadOnlyFields() {
        try {
            if (propertyValueRetriever.getPropertyValues(HRReportsConstants.ALLOW_UNIV_NUMBER_INPUT, HRReportsConstants.PROPERTY_FILE_NAME).equals("false")) {
                this.contentViewDetail.getOutputUniversityNumber().setReadOnly(true);
                this.contentViewDetail.getOutputUniversityNumber().setVisible(false);
                this.contentViewDetail.getOutputUniversityNumberLabel().setVisible(true);
            }
        } catch (IOException e) {
            throw new VaadinUIException(e, SeverityType.ERROR, null, Type.SERVERSTARTUPFAILURE, false);
        }
    }

    private boolean isCurrentEmployee() {
        Boolean currentEmployee = false;

        EmployeeDetailRequest employeeDetailRequest = new EmployeeDetailRequest();
        employeeDetailRequest.setEmployeeNumber(lookupUser);
        EmployeeAssignmentDetails employeeAssignmentDetails = hrService.getEmployeeDetail(employeeDetailRequest);

        if (!employeeAssignmentDetails.getError().getValue().equals(HRReportsMessages.getString(HRReportsConstants.MESSAGE_CATALOGUE_EMPLOYEEERRORVALUE, serviceLang))) {
            currentEmployee = true;
            employeeRecord = employeeAssignmentDetails.getEmployeeRecord().get(0);
        }
        return currentEmployee;
    }

    @Override
    public void refreshData() {
        resetUI();
        loadInitialData();
    }

    @Override
    public void populateUIComboComponents() {

        if (isCurrentEmployee()) {
            //Use utility to determine if supervisor
            if (pOSApprover.contains("Y")) {
                listOeDetail = new ArrayList<String>();
                ResponsibleOeRequest request = new ResponsibleOeRequest();
                request.setPEMPLOYEENUMBER(lookupUser);

                if (supervisorHierarchyInformationProcess != null) {
                    ResponsibleOeResult responsibleOeResult = supervisorHierarchyInformationProcess.getResponsibleOEs(request);
                    if (responsibleOeResult != null) {
                        List<ResponsibleOeDetailsType> responsibleTypes = responsibleOeResult.getResponsibleOeDetails();
                        if (responsibleTypes != null) {
                            for (ResponsibleOeDetailsType responsibleOeDetailsType : responsibleTypes) {
                                listOeDetail.add(responsibleOeDetailsType.getOeCode() + " - " + responsibleOeDetailsType.getOrganizationName());
                            }
                        }
                    }
                }

                //Get OE list from utility
                showHideParameterLabelAndOEDropDown(true);

                for (String oeCode : listOeDetail) {
                    this.contentViewDetail.getoEDropdown().addItem(oeCode);
                }

                if (!employeeRecord.getOeCode().isEmpty()) {
                    this.contentViewDetail.getoEDropdown().select(employeeRecord.getOeCode() + " - " + getUserOEDescription());
                }
            } else {
                this.contentViewDetail.getoEDropdown().addItem(employeeRecord.getOeCode());
                this.contentViewDetail.getoEDropdown().setItemCaption(employeeRecord.getOeCode(), getUserOEDescription() + " - " + employeeRecord.getOeCode());
                showHideParameterLabelAndOEDropDown(true);
                this.contentViewDetail.getoEDropdown().select(employeeRecord.getOeCode());
            }
        } else {
            throw new VaadinUIException(HRReportsMessages.getString(HRReportsConstants.MESSAGE_CATALOGUE_EMPLOYEEERROR, serviceLang), SeverityType.ERROR, Type.UNKNOWN);
        }
    }

    public String getSupervisorOE() {
        EmployeeSupervisorRequest employeeSupervisorRequest = new EmployeeSupervisorRequest();
        employeeSupervisorRequest.setPEMPLOYEENUMBER(this.lookupUser);

        EmployeeSupervisorResults employeeSupervisorResults = supervisorHierarchyInformationProcess
                .getEmployeeSupervisor(employeeSupervisorRequest);
        return employeeSupervisorResults.getSupervisorOE();
    }

    public String getPOSApprover() {
        Date date = new Date();

        PosApproverRequest posApproverRequest = new PosApproverRequest();
        posApproverRequest.setEmployeeNumber(this.lookupUser);


        PosApproverResponse posApproverResponse = supervisorHierarchyInformationProcess
                .getPOSApprover(posApproverRequest);
        return posApproverResponse.getIsSupervisor();


    }

    public String getUserOEDescription() {
        String userOEDescription = "OE";
        if (serviceLang.equals(HRReportsConstants.MESSAGE_CATALOGUE_AF_NAME)) {
            userOEDescription = employeeRecord.getOrganizationAfrName();
        }
        if (serviceLang.equals(HRReportsConstants.MESSAGE_CATALOGUE_ENG_NAME)) {
            userOEDescription = employeeRecord.getOrganizationEngName();
        }
        return userOEDescription;
    }

    public void showHideParameterLabelAndOEDropDown(Boolean show) {
        if (show) {
            if (contentViewDetail.getVerticalLayout().getComponentIndex(contentViewDetail.getReportParametersLabel()) == -1) {
                contentViewDetail.getVerticalLayout().addComponent(contentViewDetail.getReportParametersLabel(), 2);
            }
            if (contentViewDetail.getVerticalLayout().getComponentIndex(contentViewDetail.getOeHorizontalLayout()) == -1) {
                contentViewDetail.getVerticalLayout().addComponent(contentViewDetail.getOeHorizontalLayout(), 3);
            }
        } else {
            if (contentViewDetail.getVerticalLayout().getComponentIndex(contentViewDetail.getReportParametersLabel()) != -1) {
                contentViewDetail.getVerticalLayout().removeComponent(contentViewDetail.getReportParametersLabel());
            }
            if (contentViewDetail.getVerticalLayout().getComponentIndex(contentViewDetail.getOeHorizontalLayout()) != -1) {
                contentViewDetail.getVerticalLayout().removeComponent(contentViewDetail.getOeHorizontalLayout());
            }
        }
    }

    public void showHideReportTable(Boolean show) {
        if (show) {
            if (contentViewDetail.getVerticalLayout().getComponentIndex(contentViewDetail.getTableHorizontalLayout()) == -1) {
                contentViewDetail.getVerticalLayout().addComponent(contentViewDetail.getTableHorizontalLayout());
            }
        } else {
            if (contentViewDetail.getVerticalLayout().getComponentIndex(contentViewDetail.getTableHorizontalLayout()) != -1) {
                contentViewDetail.getVerticalLayout().removeComponent(contentViewDetail.getTableHorizontalLayout());
            }
        }
    }


    @Override
    public void resetUI() {
        resetNotificationPanel();
        this.contentViewDetail.getOutputTitleInitialsSurname().setValue(null);
        this.contentViewDetail.getOutputUniversityNumber().setComponentError(null);
        this.contentViewDetail.getOutputUniversityNumber().setValidationVisible(false);

        this.contentViewDetail.getOutputUniversityNumberLabel().setVisible(false);
        this.contentViewDetail.getOutputUniversityNumberLabel().setValue(null);

        this.contentViewDetail.getoEDropdown().removeAllItems();
        this.contentViewDetail.getoEDropdown().setValue(null);

        clearTable();
        showHideParameterLabelAndOEDropDown(false);
        showHideReportTable(false);
    }

    @Override
    public void mapDTOtoUIComponents() {
        contentViewDetail.getOutputUniversityNumber().setValue(lookupUser);
        contentViewDetail.getOutputUniversityNumberLabel().setValue(lookupUser);

        String titleKey = personBiographicInfo.getTitleTypeKey();
        String title = titleKey.substring(titleKey.lastIndexOf('.') + 1);
        String initials = personBiographicInfo.getInitials();
        String surname = personBiographicInfo.getLastName();
        contentViewDetail.getOutputTitleInitialsSurname().setReadOnly(false);
        contentViewDetail.getOutputTitleInitialsSurname().setValue(title + " " + initials + " " + surname);
        contentViewDetail.getOutputTitleInitialsSurname().setReadOnly(true);

        setReadOnlyFields();
        populateUIComboComponents();
    }

    @Override
    public void mapUIComponentsToDTO() {

    }

    @WebServlet(urlPatterns = "/*", name = "MyUIServlet", asyncSupported = true)
    @VaadinServletConfiguration(ui = HRReportsSPCrudUI.class, productionMode = false)
    public static class MyUIServlet extends VaadinServlet {
    }

    public void createTable() {
        contentViewDetail.getTableHorizontalLayout().setSizeUndefined();
        contentViewDetail.getReportSelectionTable().removeAllItems();
        contentViewDetail.getReportSelectionTable().setSelectable(false);
        contentViewDetail.getReportSelectionTable().setSizeFull();
        contentViewDetail.getReportSelectionTable().addContainerProperty(HRReportsMessages.getString(HRReportsConstants.MessageCatalogue.REPORT_COLUMN_NAME, serviceLang), Label.class, null);
        contentViewDetail.getReportSelectionTable().addContainerProperty(HRReportsMessages.getString(HRReportsConstants.MessageCatalogue.SELECT_COLUMN_NAME, serviceLang), CheckBox.class, null);

        contentViewDetail.getReportSelectionTable().setColumnAlignment(HRReportsMessages.getString(HRReportsConstants.MessageCatalogue.SELECT_COLUMN_NAME, serviceLang), Table.Align.CENTER);

        refreshTableSize();
    }

    public void populateTable() {
        HRLookupInformationRequest hrLookupInformationRequest = new HRLookupInformationRequest();
        hrLookupInformationRequest.setLookupType(HRReportsMessages.getString(HRReportsConstants.MESSAGE_CATALOGUE_LOOKUPTYPE, serviceLang));
        hrLookupInformationRequest.setLanguage(HRReportsMessages.getString(HRReportsConstants.MESSAGE_CATALOGUE_LOOKUPLANGUAGE, serviceLang));
        HRCOILookupInformation hrcoiLookupInformation = hrService.getHRCodeLookups(hrLookupInformationRequest);
        if (hrcoiLookupInformation.isError()) {
            throw new VaadinUIException(hrcoiLookupInformation.getErrorMessage().getValue(), SeverityType.ERROR, Type.SERVERSIDE);
        } else {
            int rowCounter = 0;
            for (HRCOILookupInformation.LookupRecord report : hrcoiLookupInformation.getLookupRecord()) {
                rowCounter++;
                if (pOSApprover.contains("Y")) {
                    if (report.getLookupCode().endsWith("Supervisor")) {
                        contentViewDetail.getReportSelectionTable().addItem(new Object[]{getReport(rowCounter,
                                report.getLookupCode()), getSelect(rowCounter)}, rowCounter);
                    }
                } else if (report.getLookupCode().endsWith("All")) {
                    contentViewDetail.getReportSelectionTable().addItem(new Object[]{getReport(rowCounter,
                            report.getLookupCode()), getSelect(rowCounter)}, rowCounter);
                }

            }
        }
        refreshTableSize();
    }

    public Label getReport(int rowID, String value) {
        value = value.substring(0, value.lastIndexOf('.'));
        Label reportLabel = new Label(value);
        reportLabel.setId(String.valueOf(rowID));
        return reportLabel;
    }

    public CheckBox getSelect(int rowID) {
        CheckBox selectCheckBox = new CheckBox();
        selectCheckBox.setId(String.valueOf(rowID));

        selectCheckBox.addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            public void valueChange(Property.ValueChangeEvent event) {
                Boolean selectedValue = (Boolean) event.getProperty().getValue();
                int id = Integer.parseInt(((Field.ValueChangeEvent) event).getComponent().getId());
            }
        });

        return selectCheckBox;
    }

    public Label getLabel(int rowID, String columnName) {
        return (Label) contentViewDetail.getReportSelectionTable()
                .getContainerProperty(rowID, columnName)
                .getValue();
    }

    public String getLabelValue(int rowID, String columnName) {
        String value = getLabel(rowID, columnName).getValue();
        return value;
    }

    public CheckBox getSelectCheckBox(int rowID, String columnName) {
        return (CheckBox) contentViewDetail.getReportSelectionTable()
                .getContainerProperty(rowID, columnName)
                .getValue();
    }

    public Boolean getSelectCheckBoxValue(int rowID, String columnName) {
        Boolean value = getSelectCheckBox(rowID, columnName).getValue();
        return value;
    }

    public void refreshTableSize() {
        contentViewDetail.getReportSelectionTable().setPageLength(contentViewDetail.getReportSelectionTable().getContainerDataSource().getItemIds().size());
    }

    public void clearTable() {
        this.contentViewDetail.getReportSelectionTable().removeAllItems();
        refreshTableSize();
    }

    public String getHRServiceRegistryEnvironmentTypeKey(String serviceName) {

        String serviceLookupKey;

        serviceLookupKey = "/" + appRuntimeEnvironment + "/" + serviceName;
        System.out.println("Service lookup key" + serviceLookupKey.toUpperCase());
        return serviceLookupKey.toUpperCase();
    }


    @Override
    public String getServiceRegistryEnvironmentTypeKey(String s) {
        //
//    	/TEST/IAPI-PERSONSERVICE/V3/V_TEST
        //

        String DB = null;
        String serviceLookupKey;
        if (appRuntimeEnvironment.equals("test")) {
            DB = "/V_TEST";
        } else if (appRuntimeEnvironment.equals("qa")) {
            DB = "/V_TEST";
        }

        if (DB != null) {
            serviceLookupKey = "/" + appRuntimeEnvironment + "/IAPI-" + s + "/V3" + DB;
        } else {
            serviceLookupKey = "/" + appRuntimeEnvironment + "/IAPI-" + s + "/V3";
        }
        System.out.println("Service lookup key" + serviceLookupKey.toUpperCase());
        return serviceLookupKey.toUpperCase();
    }

    @Override
    public void setAppPropertyFileName() {
        // TODO Auto-generated method stub

    }

    @Override
    public void readApplicationPropertyFile() {
        // TODO Auto-generated method stub

    }

    public static String getMajorVersion(String version) {
        String[] output = version.split("\\.");
        String majorVersion = "V" + output[0];
        return majorVersion;
    }

}