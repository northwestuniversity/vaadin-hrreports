package nwu.ac.za.util;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertyValueRetriever {

    public String getPropertyValues(String key, String propertyFileName) throws IOException {
        InputStream inputStream = null;
        String results = "";

        try {
            Properties properties = new Properties();
            String propertiesFileName = propertyFileName;

            inputStream = getClass().getClassLoader().getResourceAsStream(propertiesFileName);

            if (inputStream != null) {
                properties.load(inputStream);
            } else {
                throw new FileNotFoundException("property file '" + propertiesFileName + "'not found in the classpath");
            }

            results = properties.getProperty(key);

        } catch (Exception e) {
            System.out.println("Exception: " + e);
        } finally {
        	if (inputStream != null) {
                inputStream.close();				
			}
        }
        return results;
    }


}
