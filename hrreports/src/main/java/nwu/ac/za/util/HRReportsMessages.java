package nwu.ac.za.util;

/**
 * Created by NWUUSER on 2017/08/10.
 */

import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

public class HRReportsMessages {
    private static final String BUNDLE_NAME = "nwu/ac/za/messages/HRReportsMessages";


    private HRReportsMessages() {
    }

    public static String getString(String key) {
        try {
            System.out.println("Default = " + Locale.getDefault());
            ResourceBundle myResourceBundle = ResourceBundle.getBundle(BUNDLE_NAME + "_" + Locale.getDefault());
            Locale.getDefault();
            return myResourceBundle.getString(key);
        } catch (MissingResourceException e) {
            return '!' + key + '!';
        }
    }

    public static String getString(String key, String fileName) {
        try {
            System.out.println("Default = " + Locale.getDefault());
            ResourceBundle myResourceBundle = ResourceBundle.getBundle(BUNDLE_NAME + fileName);
            Locale.getDefault();
            return myResourceBundle.getString(key);
        } catch (MissingResourceException e) {
            return '!' + key + '!';
        }
    }
}
