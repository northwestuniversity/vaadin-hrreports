package nwu.ac.za.util;

import nwu.ac.za.framework.utility.PropertyValueRetriever;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class HRReportsConstants {

    private static final Logger log = LoggerFactory.getLogger(HRReportsConstants.class.getName());

    //WSDL service property names
    public static final String PROPERTY_FILE_NAME = "config.properties";
    public static final String USER_NAME = "username";
    public static final String PASSWORD = "password";
    public static final String ALLOW_UNIV_NUMBER_INPUT = "allowUnivNumberInput";
    public static final String RUN_ENVIRONMENT = "runEnvironment";

    public static class MessageCatalogue {
        //Component Text
        //Table Column Name
        public static final String REPORT_COLUMN_NAME = "reportColumn";
        public static final String SELECT_COLUMN_NAME = "selectColumn";
    }

    //Message Catalogue
    //File names
    public static final String MESSAGE_CATALOGUE_AF_NAME = "_af_ZA";
    public static final String MESSAGE_CATALOGUE_ENG_NAME = "_en_ZA";
    //Constants
    public static final String MESSAGE_CATALOGUE_REPORTSUCCESS = "reportSuccess";
    public static final String MESSAGE_CATALOGUE_REPORTERROR = "reportError";
    public static final String MESSAGE_CATALOGUE_EMPLOYEEERROR = "employeeError";
    public static final String MESSAGE_CATALOGUE_LOOKUPTYPE = "lookupType";
    public static final String MESSAGE_CATALOGUE_LOOKUPLANGUAGE = "lookupLanguage";
    public static final String MESSAGE_CATALOGUE_NOREPORTS = "noReports";
    public static final String MESSAGE_CATALOGUE_EMPLOYEEERRORVALUE = "employeeErrorValue";

    //App Browser Tab Name
    public static final String APP_NAME = "HR DIY Reporting";

    //Language for view
    public static final String APP_LANGUAGE_AF = "af";
    
    public static final String WS_IAPI_READ_USER_NAME = "ws_iapiapp_read_username";
    public static final String WS_IAPI_READ_USER_NAME_PASSWORD = "ws_iapiapp_read_username_password";
    public static final String PROPERTY_NAME_IDENTITY_API_VERSION = "identity.api.version";

    public static final String HR_EMPLOYEE_INFO_SERVICE_VERSION = "hr-employeeinformation-service.version";

    public static String getHR_EMPLOYEE_INFO_SERVICE_VERSION(){
        try {
            return getPropertyValue(HR_EMPLOYEE_INFO_SERVICE_VERSION, PROPERTY_FILE_NAME);
        } catch (IOException e) {
            log.error(e.getLocalizedMessage(), e);
        }
        return null;
    }


    public static String getPropertyValue(String key, String propertyFileName) throws IOException {
        InputStream inputStream = null;
        String results = "";

        try {
            Properties properties = new Properties();
            inputStream = PropertyValueRetriever.class.getClassLoader().getResourceAsStream(propertyFileName);
            if (inputStream == null) {
                throw new FileNotFoundException("property file '" + propertyFileName + "'not found in the classpath");
            }

            properties.load(inputStream);
            results = properties.getProperty(key);
        } catch (Exception var9) {
            System.out.println("Exception: " + var9);
        } finally {
            if (inputStream != null) {
                inputStream.close();
            }

        }

        return results;
    }
}
